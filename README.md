rucio\_dump is a tool to download and work with Rucio dumps.

Currently it allows to download the dumps available in
https://rucio-ui.cern.ch/dumps

Installation
------------

```sh
git clone https://gitlab.cern.ch/felopez/rucio-dumps.git
cd rucio-dumps
virtualenv .
. bin/activate
pip install -r requirements
```

Binary distribution with PyInstaller
------------------------------------

In order to allow the use of the rucio_dump without any setup is possible to
build a binary distribution with all dependencies included.

To create the binary distribution follow the steps in the installation section
and then run:

```sh
pip install pyinstaller
scripts/pyinstaller
```

The binary program will be located on `dist/rucio_dump`.

Printing a dump
---------------

Currently the following dumps can be downloaded:
- Replicas dumps: dump-replicas
- Dataset dumps: dump-datasets
- Complete datasets: dump-complete-datasets

The following command tries to download the lastest dataset dump for the
given DDM Endpoint:
```sh
./rucio_dump.py dump-datasets <DDM_ENDPOINT>
```

The date can be specified:
```sh
./rucio_dump.py dump-datasets --date 20-07-2015 <DDM_ENDPOINT>
```

Output formats
--------------

The dumps can be printed in different formats with `--csv`, `--csv-nohead`
and `--tabulate <fmt>`. `--tabulate` is the slowest option as it loads
the full dump in memory to compute the width of each column.

Filtering
---------

Basic filtering by column is allowed with `--filter=<column>=<value>[,<column>=<value>,...]`.

To get the name of the columns for a given type of dump use the format CSV
with headers and take the first line:
```sh
./rucio_dump.py dump-datasets --csv --date 20-07-2015 <DDM_ENDPOINT> | head -n 1
```

Hidding/Showing columns
----------------------

`--hide <column_name>[,<column_name>,...]` will hide the specified column(s),
while `--columns <column_name>[,<column_name>,...]` only shows the specified
column(s).

Data aggregation
----------------

Currently only the sum function is supported for data aggregation, as in
SQL first the data must be grouped and the aggregation function runs in
each group.

For example to calculate the total size of datasets grouped by state:
```sh
./rucio_dump.py --group-by state --sum size dump-datasets CERN-PROD_DATADISK
```

Consistency checks
------------------

As described in https://twiki.cern.ch/twiki/bin/view/AtlasComputing/DDMDarkDataAndLostFiles
rucio_dump can run consistency checks to find dark data and lost files.

There are three different ways to run the checks:

- Automatic download of Rucio dumps delta days older and delta days newer than
the storage dump:

```
./rucio_dumps.py consistency --delta <days> <DDM_ENDPOINT_NAME> dump_YYYYMMDD > output
```

- Automatic download of Rucio dumps specifying the date of each dump:

```
./rucio_dumps.py consistency --prev-date DD-MM-YYYY --next-date DD-MM-YYYY <DDM_ENDPOINT_NAME> dump_YYYYMMDD > output
```

- Check with the Rucio dump files specified in the arguments:

```
./rucio_dumps.py consistency-manual --replicas_before <replicas_dump> --replicas_after <replicas_dump> <DDM_ENDPOINT_NAME> dump_YYYYMMDD > output
```

The output of the script has the format:

```
<STATUS>,<PATH>
```

Where `<STATUS>` is either `DARK` or `LOST`.

The results of this script should be checked using Rucio to verify darks and GFAL to verify lost files.

Details in the parameters to be used and in the way to create the SE dump
are available in
https://twiki.cern.ch/twiki/bin/view/AtlasComputing/DDMDarkDataAndLostFiles.
