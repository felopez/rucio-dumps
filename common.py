import ConfigParser
import contextlib
import datetime
import importlib
import logging
import os
import re
import requests
import sys
import tempfile
import json

logger = logging.getLogger('rucio_dumps')

# bz2 module doesn't support multi-stream files in Python < 3.3
# https://bugs.python.org/issue1625
# using bz2file (a backport from the lastest CPython bz2 implementation)
# fixes this problem
import bz2file

# Ugly fix to support pyinstaller packaging
# https://github.com/pyinstaller/pyinstaller/issues/885
if sys.getfilesystemencoding() is None:
    sys.getfilesystemencoding = lambda: 'UTF-8'

import magic



# Load only installed optional modules
optional_modules = {
    #'rucio': ('rucio.client', 'rucio.common.exception'),
    'tabulate': ('tabulate',)
}

for flag, modules in optional_modules.items():
    globals()['_'.join(('option', flag))] = True
    for module in modules:
        try:
            optional_modules[module] = importlib.import_module(module)
        except ImportError:
            logger.warn('Error importing "%s", some features may be disabled', module)
            globals()['_'.join(('option', flag))] = False


def error(text, exit_code=1):
    '''
    Log and print `text` error. This function ends the execution of the program with exit code
    `exit_code` (defaults to 1).
    '''
    logger.error(text)
    sys.stderr.write(text + '\n')
    exit(1)


DUMPS_CACHE_DIR = 'cache'
CHUNK_SIZE = 4194304  # 4MiB

rucio_home = os.environ.get('RUCIO_HOME', None)

if rucio_home is not None:
    rucio_config_file = os.path.join(rucio_home, 'etc', 'rucio.cfg')
    config = ConfigParser.ConfigParser()
    config.read(rucio_config_file)
    try:
        cacert = config.get('client', 'ca_cert').replace('$RUCIO_HOME', rucio_home)
    except KeyError:
        cacert = None

    if cacert is None or not os.path.exists(cacert):
        logger.warn('Configured CA Certificate file "%s" not found: Host certificate verification disabled', cacert)
        cacert = False

else:
    logger.warn('$RUCIO_HOME not set: Host certificate verification disabled')
    cacert = False


session = requests.Session()
session.verify = cacert
session.stream = True

# FIXME: END

# There are two Python modules with the name `magic`, luckily both do
# the same thing.
if 'open' in dir(magic):
    _mime = magic.open(magic.MAGIC_MIME)
    _mime.load()
    mimetype = _mime.file
else:
    mimetype = lambda filename: magic.from_file(filename, mime=True)


def isplaintext(filename):
    '''
    Returns True if `filename` has mimetype == 'text/plain'.
    '''
    if os.path.islink(filename):
        filename = os.readlink(filename)
    return mimetype(filename).split(';')[0] == 'text/plain'


def smart_open(filename):
    '''
    Returns an open file object if `filename` is plain text, else assumes
    it is a bzip2 compressed file and returns a file-like object to
    handle it.
    '''
    if isplaintext(filename):
        f = open(filename, 'rt')
    else:
        f = bz2file.open(filename, 'rt')
    return f


@contextlib.contextmanager
def temp_file(directory, name=None, final_name=None):
    '''
    Allows to create a temporal file to store partial results, when the
    file is complete it is renamed to `final_name`.

    - `directory`: working path to create the temporal and the final file.
    - `name`: Path of the temporal file, relative to `directory`.
       If empty a random name is generated.
    - `final_name`: Path of the final file, relative to `directory`.
       If empty the renaming step is ommited, leaving the temporal file with
       the results.

    Important: `name` and `final_name` must be in the same filesystem as
    a hardlink is used to rename the temporal file.
    '''
    if name is None:
        fd, tpath = tempfile.mkstemp(dir=directory)
        tmp = os.fdopen(fd, 'w')
    else:
        tpath = os.path.join(directory, name)
        tmp = open(tpath, 'w')

    yield tmp

    tmp.close()
    if final_name is not None:
        dst_path = os.path.join(directory, final_name)
        logger.debug('Renaming "%s" to "%s"', tpath, dst_path)
        os.link(tpath, dst_path)
        os.unlink(tpath)


DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
DATETIME_FORMAT_FULL = '%Y-%m-%dT%H:%M:%S'
MILLISECONDS_RE = re.compile(r'\.(\d{3})Z$')


def to_datetime(str_or_datetime):
    """
    Convert string to datetime. The format is somewhat flexible.
    Timezone information is ignored.
    """
    if isinstance(str_or_datetime, datetime.datetime):
        return str_or_datetime
    elif str_or_datetime.strip() == '':
        return None

    str_or_datetime = str_or_datetime.replace('T', ' ')
    try:
        logger.debug(
            'Trying to parse "%s" date with '
            'resolution of seconds or tenths of seconds',
            str_or_datetime,
        )
        str_or_datetime = re.sub(r'\.\d$', '', str_or_datetime)
        date = datetime.datetime.strptime(
            str_or_datetime,
            DATETIME_FORMAT,
        )
    except ValueError:
        logger.debug(
            'Trying to parse "%s" date with resolution of milliseconds',
            str_or_datetime,
        )
        miliseconds = int(MILLISECONDS_RE.search(str_or_datetime).group(1))
        str_or_datetime = MILLISECONDS_RE.sub('', str_or_datetime)
        date = datetime.datetime.strptime(
            str_or_datetime,
            DATETIME_FORMAT,
        )
        date = date + datetime.timedelta(microseconds=miliseconds * 1000)
    return date

def agis_endpoints_data():
    agis_data = getattr(agis_endpoints_data, '__agis_data', None)
    if agis_data is None:
        ddmendpoints_data_url = ('http://atlas-agis-api.cern.ch/request/ddmendpoint'
                                '/query/list/?json')

        r = requests.get(ddmendpoints_data_url)
        assert r.status_code == 200
        agis_data = json.loads(r.text)
        agis_endpoints_data.__agis_data = agis_data
    return agis_data
