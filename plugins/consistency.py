# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Fernando Lopez, <felopez@cern.ch>, 2015
from common import error, DUMPS_CACHE_DIR
import common
import data_models
import datetime
import logging
import os
import path_parsing
import random
import re
import string
import subprocess
import tempfile

logger = logging.getLogger('rucio_dumps')

subcommands = ['consistency', 'consistency-manual']


class RecordType(data_models.DataModel):
    _date_re = re.compile(r'dump_(\d{8})')
    SCHEMA = (
        ('apparent_status', str),
        ('path', str),
    )

    @classmethod
    def parse_args(cls, args):
        cls.ddm_endpoint = args.ddm_endpoint
        cls.storage_dump = args.storage_dump
        date_str = cls._date_re.match(os.path.basename(cls.storage_dump))
        if date_str is None:
            error('The storage dump filename must be of the form '
                  '"dump_YYYYMMDD" where the date correspond to the date '
                  'of the newest files included')
        date_str = date_str.group(1)
        assert date_str is not None
        try:
            cls.date = datetime.datetime.strptime(date_str, '%Y%m%d')
        except ValueError:
            error('Invalid date {0}'.format(date_str))

        if not os.path.exists(cls.storage_dump):
            error('File "{0}" does not exist'.format(cls.storage_dump))

        if (args.prev_date is not None or args.next_date is not None) and args.delta is not None:
            error('Missing or conflicting arguments, specify either '
                  '"--delta" or "--prev-date" and "--next-date"')

        if args.prev_date is not None and args.next_date is not None:
            cls.prev_date = datetime.datetime.strptime(
                args.prev_date, '%d-%m-%Y',
            )
            cls.next_date = datetime.datetime.strptime(
                args.next_date, '%d-%m-%Y',
            )
        elif args.delta is not None:
            delta = int(args.delta)
            cls.prev_date = cls.date - datetime.timedelta(days=delta)
            cls.next_date = cls.date + datetime.timedelta(days=delta)
        else:
            error('Missing arguments, specify either "--delta" or '
                  '"--prev-date" and "--next-date"')

        cls.sort_rucio_replica_dumps = args.sort_rucio_dumps

    @classmethod
    def parse_args_manual(cls, args):
        cls.storage_dump = args.storage_dump
        cls.prev_date_fname = args.replicas_before
        cls.next_date_fname = args.replicas_after

        for path in [cls.storage_dump, cls.prev_date_fname, cls.next_date_fname]:
            if not os.path.exists(path):
                error('File "{0}" does not exist'.format(path))

    @classmethod
    def dump(cls, args):
        if args.subcommand == 'consistency':
            cls.parse_args(args)
            cls.prev_date_fname = data_models.Replica.download(
                cls.ddm_endpoint, cls.prev_date)
            cls.next_date_fname = data_models.Replica.download(
                cls.ddm_endpoint, cls.next_date)
            assert cls.prev_date_fname is not None
            assert cls.next_date_fname is not None
        else:
            cls.parse_args_manual(args)
            cls.ddm_endpoint = '{0}_{1}_{2}'.format(
                'UNKNOWN_ENDPOINT',
                ''.join(
                    [random.choice(string.ascii_lowercase) for x in range(20)]
                ),
                os.path.basename(cls.storage_dump),
            )

        def parser(line):
            fields = line.split('\t')
            path = fields[6].strip().lstrip('/')
            status = fields[8].strip()

            return ','.join((path, status))

        prefix = path_parsing.prefix(
            common.agis_endpoints_data(),
            cls.ddm_endpoint,
        )

        prefix_components = path_parsing.components(prefix)

        def strip_storage_dump(line):
            relative = path_parsing.remove_prefix(
                prefix_components,
                path_parsing.components(line),
            )
            if relative[0] == 'rucio':
                relative = relative[1:]
            return '/'.join(relative)

        if cls.sort_rucio_replica_dumps:
            prev_date_fname_sorted = gnu_sort(
                parse_and_filter_file(cls.prev_date_fname, parser=parser),
                delimiter=',',
                fieldspec='1',
            )

            next_date_fname_sorted = gnu_sort(
                parse_and_filter_file(cls.next_date_fname, parser=parser),
                delimiter=',',
                fieldspec='1',
            )
        else:
            prev_date_fname_sorted = parse_and_filter_file(
                cls.prev_date_fname,
                parser=parser,
                postfix='sorted',
            )
            next_date_fname_sorted = parse_and_filter_file(
                cls.next_date_fname,
                parser=parser,
                postfix='sorted',

            )


        sd_prefix = 'ddmendpoint_{0}_{1}'.format(
            cls.ddm_endpoint,
            cls.date.strftime('%d-%m-%Y'),
        )
        storage_dump_fname_sorted = gnu_sort(
            parse_and_filter_file(
                cls.storage_dump,
                parser=strip_storage_dump,
                prefix=sd_prefix,
            ),
            prefix=sd_prefix,
        )

        with open(prev_date_fname_sorted) as prevf:
            with open(next_date_fname_sorted) as nextf:
                with open(storage_dump_fname_sorted) as sdump:
                    for path, where, status in compare3(prevf, sdump, nextf):
                        prevstatus, nextstatus = status

                        if where[0] and not where[1] and where[2]:
                            if prevstatus == 'A' and nextstatus == 'A':
                                yield cls('LOST', path)

                        if not where[0] and where[1] and not where[2]:
                            yield cls('DARK', path)


def _try_to_advance(it, default=None):
    try:
        el = it.next()
    except StopIteration:
        return default
    return el.strip()


def min3(*values):
    '''
    Minimum between the 3 values ignoring None
    '''
    values = [value.split(',')[0] for value in values if value is not None]
    assert len(values) > 0
    return min(values)


def compare3(it0, it1, it2):
    '''
    Generator to compare 3 sorted iterables, in each
    iteration it yields a tuple of the form (current, (bool, bool, bool))
    where current is the current element checked and the
    second element of the tuple is a triplet whose elements take
    a true value if current is contained in the it0, it1 or it2
    respectively.

    This function can't compare the iterators properly if None is
    a valid value.
    '''

    it0 = iter(it0)
    it1 = iter(it1)
    it2 = iter(it2)
    v0 = _try_to_advance(it0)
    v1 = _try_to_advance(it1)
    v2 = _try_to_advance(it2)

    while v0 is not None or v1 is not None or v2 is not None:
        vmin = min3(v0, v1, v2)
        in0 = False
        in1 = False
        in2 = False
        s0 = s2 = None

        # Detect in which iterables the value is present
        #   inN is True if the value is present on the N iterable.
        #   sN  is the status of the path in the rucio replica
        #       dumps (N is either 0 or 2).
        if v0 is not None and v0.split(',')[0] == vmin:
            in0 = True
            s0 = v0.split(',')[1]

        if v1 is not None and v1 == vmin:
            in1 = True

        if v2 is not None and v2.split(',')[0] == vmin:
            in2 = True
            s2 = v2.split(',')[1]

        # yield the value, in which iterables is present, and the status
        # in each rucio replica dumps (if it is present there, else None).
        yield (vmin, (in0, in1, in2), (s0, s2))

        # Discard duplicate entries (it shouldn't be duplicate entries
        # anyways) and
        # advance the iterators, if the iterator N is depleted vN is set
        # to None.
        while v0 is not None and v0.split(',')[0] == vmin:
            in0 = True
            v0 = _try_to_advance(it0)

        while v1 is not None and v1 == vmin:
            in1 = True
            v1 = _try_to_advance(it1)

        while v2 is not None and v2.split(',')[0] == vmin:
            in2 = True
            v2 = _try_to_advance(it2)


def parse_and_filter_file(filepath, parser=lambda s: s, filter_=lambda s: s, prefix=None, postfix='parsed'):
    prefix = os.path.basename(filepath) if prefix is None else prefix
    output_name = '_'.join((prefix, postfix))
    output_path = os.path.join(DUMPS_CACHE_DIR, output_name)

    if os.path.exists(output_path):
        return output_path

    with common.temp_file(DUMPS_CACHE_DIR, final_name=output_name) as output:
        input_ = common.smart_open(filepath)
        for line in input_:
            if filter_(line):
                output.write(parser(line) + '\n')

        input_.close()

    return output_path

def gnu_sort(file_path, prefix=None, delimiter=None, fieldspec=None):
    assert (delimiter is None and fieldspec is None) or (delimiter is not None and fieldspec is not None)
    if delimiter is None:
        cmd_line = 'LC_ALL=C sort {0} > {1}'
    else:
        cmd_line = 'LC_ALL=C sort -t {0} -k {1} {{0}} > {{1}}'.format(delimiter, fieldspec)

    prefix = os.path.basename(file_path) if prefix is None else prefix

    sorted_name = '_'.join((prefix, 'sorted'))
    sorted_path = os.path.join(DUMPS_CACHE_DIR, sorted_name)

    if os.path.exists(sorted_path):
        return sorted_path

    # FIXME: mktemp() is an insecure function and this may be a security
    # threat in some scenarios. Find another way to do it.
    tfile = tempfile.mktemp(dir=DUMPS_CACHE_DIR)

    subprocess.check_call(
        cmd_line.format(file_path, tfile),
        shell=True,
    )

    os.link(tfile, sorted_path)
    os.unlink(tfile)

    return sorted_path


def populate_args(argparser):
    # Option to download the rucio replica dumps automaticaly
    parser = argparser.add_parser(
        'consistency',
        help='Consistency check to verify possible lost files and dark data '
             '(replica dumps are downloaded automatically)'
    )
    parser.add_argument('ddm_endpoint')
    parser.add_argument('storage_dump')
    parser.add_argument(
        '--delta',
        help='Difference in days between the SE dump and the desired rucio '
             'replica dumps (use either this argument or --prev-date and '
             '--next-date)',
        required=False
    )
    parser.add_argument(
        '--prev-date',
        help='Date of the older rucio replica dump to use',
        required=False
    )
    parser.add_argument(
        '--next-date',
        help='Date of the newer rucio replica dump to use',
        required=False
    )

    # Option to use already downloaded rucio replica dumps
    parser_manual = argparser.add_parser(
        'consistency-manual',
        help='Consistency check to verify possible lost files and dark data '
             '(replica dumps should be provided by the user)'
    )
    parser_manual.add_argument('replicas_before')
    parser_manual.add_argument('storage_dump')
    parser_manual.add_argument('replicas_after')

    for p in (parser, parser_manual):
        p.add_argument(
            '--sort-rucio-dumps',
            help='Starting 18-08-2015 the Rucio Replica Dumps are sorted by '
                 'path. If you need to work with older dumps use this '
                 'argument.',
            action='store_true'
        )
